// Синхронным называется код, когда программа последовательно выполняет JavaScript код, 
// от строчки к строчке.
// Асинхронность - это когда код выполняется не последовательно, строчка кода, 
// стоящая ниже, выполнится раньше верхней строчки. 


function createButton() {
  const newButton = document.createElement('button')
  newButton.innerText = 'Вычислить по IP';
  document.body.append(newButton);
  newButton.addEventListener('click', async (e) => {
    const ul = document.querySelector('ul')
    if(ul){
      ul.remove()
    }
    const resp = await fetch(`https://api.ipify.org/?format=json`)
    const data = await resp.json();
    const ipResp = await fetch(`http://ip-api.com/json/${data.ip}?fields=continent,country,region,city,district`)
    const ipData = await ipResp.json();
    console.log(ipData);
    list(ipData)
  })
}

function list(ipData){
const {city, continent, country, district, region} = ipData;
const html = `
<ul>
<li>Континент: ${continent}</li>
<li>Страна: ${country}</li>
<li>Город: ${city}</li>
<li>Регион: ${region}</li>
<li>Район: ${district || 'no data available'}</li>
</ul>
`
document.body.insertAdjacentHTML('beforeend', html);
}

createButton()

